//Seyed Amirreza Mojtahedi 2133044
package bicycle;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    public Bicycle(String manufacturer,int numberGears,double maxSpeed){
        this.manufacturer=manufacturer;
        this.numberGears=numberGears;
        this.maxSpeed=maxSpeed;
    }
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }
    public String toString(){
        return "Manufacturer: "+manufacturer+"\nnumber of gears: "+numberGears+"\nmax speed: "+maxSpeed;
    }
}