//Seyed Amirreza Mojtahedi 2133044
package application;

import bicycle.Bicycle;

public class BikeStore {
    public static void main(String[] main){
        Bicycle[] bikeArray=new Bicycle[4];
        bikeArray[0]= new Bicycle("Yamaha", 7, 25);
        bikeArray[1]= new Bicycle("BMW", 5, 50);
        bikeArray[2]= new Bicycle("Benz", 3, 40);
        bikeArray[3]= new Bicycle("Bixi", 7, 20);
        for(Bicycle counter : bikeArray){
            System.out.println(counter);
        }
    }
}
